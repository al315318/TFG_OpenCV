#include "opencv2/opencv.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <windows.h>
#include <algorithm>
#include <fstream>
#include <iostream>

using namespace std;
using namespace cv;

VideoCapture capture;

void setLabelCircle(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255, 255, 255), CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);
}

struct strX{
	bool operator() ( Point a, Point b ){
		return a.x < b.x;
	}
} compX;

struct strY{
	bool operator() ( Point a, Point b ){
		return a.y < b.y;
	}
} compY;

//THE ORDER IS IMPORTANT :
//1. TOP_LEFT, 2. TOP_RIGHT, 3. BOTTOM_RIGHT, 4. BOTTOM_LEFT
vector<Point> orderPoints(vector<Point> pts)
{
	//Initialize list of points
	vector<Point> rect(4);
	vector<Point> ptsXSorted(pts);

	//Sort points using X coord
	std::sort(ptsXSorted.begin(), ptsXSorted.end(), compX);

	//Divide the points into two: left & right
	vector<Point> leftMost(ptsXSorted.begin(), ptsXSorted.begin()+2);
	vector<Point> rightMost(ptsXSorted.begin()+2, ptsXSorted.end());

	//Sort the left points using Y coord to obtain the top-left and bottom-left
	vector<Point> leftMostYSorted(leftMost);
	std::sort(leftMostYSorted.begin(), leftMostYSorted.end(), compY);

	rect[0] = leftMostYSorted[0];
	rect[3] = leftMostYSorted[1];

	//Sort the right points using Y coord to obtain the top-right and bottom-right
	vector<Point> rightMostYSorted(rightMost);
	std::sort(rightMostYSorted.begin(), rightMostYSorted.end(), compY);

	rect[1] = rightMostYSorted[0];
	rect[2] = rightMostYSorted[1];

	return rect;
}

int findGrid(Mat frame, int XDiv, int YDiv, int amount){
	Mat gray;
	//Image to grayscale and blur
	cvtColor(frame, gray, CV_BGR2GRAY);
	GaussianBlur(gray, gray, Size(9, 9), 2, 2);

	vector<cv::Vec3f> circles;

	//The last two parameters allow finding smaller circles
	HoughCircles(gray, circles, CV_HOUGH_GRADIENT, 1, gray.rows/2, 100, 70);

	//If there is not the expected amount do nothing
	if (circles.size()!=4)
	{
		return -1;
	}

	//Draw circles and store them in corners (not in order)
	vector<Point> corners;
	for ( size_t i=0; i<circles.size(); i++)
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);
		circle(frame, center, 3, Scalar(0, 255, 0), 3);
		circle(frame, center, radius, Scalar(0, 0, 255), 3);

		corners.push_back(Point( center.x, center.y));
	}

	//Sort the corners (it's important that they follow an order)
	vector<Point> sortedCorners = orderPoints(corners);

	Point topLeft = sortedCorners[0];
	Point topRight = sortedCorners[1];
	Point bottomRight = sortedCorners[2];
	Point bottomLeft = sortedCorners[3];

	//Compute the width of the new image, which will be the
	//maximum distance between bottom-right and bottom-left
	//x-coordiates or the top-right and top-left x-coordinates
	int widthA = sqrt(pow(bottomRight.x-bottomLeft.x, 2) + pow(bottomRight.y-bottomLeft.y, 2));
	int widthB = sqrt(pow(topRight.x-topLeft.x, 2) + pow(topRight.y-topLeft.y, 2));
	int maxWidth = max(widthA, widthB);

	//Compute the height of the new image, which will be the
	//maximum distance between the top-right and bottom-right
	//y-coordinates or the top-left and bottom-left y-coordinates
	int heightA = sqrt(pow(topRight.x-bottomRight.x, 2) + pow(topRight.y-bottomRight.y, 2));
	int heightB = sqrt(pow(topLeft.x-bottomLeft.x, 2) + pow(topLeft.y-bottomLeft.y, 2));
	int maxHeight = max(heightA, heightB);

	//Construct the set of destination points to obtain a top-down view
	vector<Point> dst;
	dst.push_back(Point(0, 0));
	dst.push_back(Point(maxWidth-1, 0));
	dst.push_back(Point(maxWidth-1, maxHeight-1));
	dst.push_back(Point(0, maxHeight-1));

	//Convert the vectors to arrays and Point type to Point2f.
	//GetPerspectiveTransform needs Point2f array.
	Point2f sortedCornersArray[4];
	copy(sortedCorners.begin(), sortedCorners.end(), sortedCornersArray);
	Point2f dstArray[4];
	copy(dst.begin(), dst.end(), dstArray);

	//Compute the perspective transform matrix and then apply it
	Mat persM = getPerspectiveTransform(sortedCornersArray, dstArray);
	Mat warped;
	warpPerspective(frame, warped, persM, Size(maxWidth, maxHeight));

	//Convert the image to grayscale and blur it to avoid obtaining false negative
	Mat warpedGray;
	cvtColor(warped, warpedGray, CV_BGR2GRAY);
	GaussianBlur(warpedGray, warpedGray, Size(9, 9), 2, 2);

	ofstream outfile;
	outfile.open(".\\Assets\\Resources\\Maps\\CVMap.txt");
	int rows =warpedGray.rows;
	int cols =warpedGray.cols;

	int x = 0;
	int y = 0;
	int foundBlack = 0;

	//Iterate the image pixels that represent each rectangle in the grid
	if (outfile.is_open())
	{
		for (int i = 1; i <= YDiv; i++)
		{
			y = ((float(i)/float(YDiv))*rows)-((float(rows)/float(YDiv))/2);

			for (int j = 2; j < XDiv; j++)
			{
				x = ((float(j)/float(XDiv))*cols)-((float(cols)/float(XDiv))/2);

				uchar intensity = warpedGray.at<uchar>(y, x);

				//If is black
				if(intensity<100)
				{
					outfile << "X";
					foundBlack++;
				}else
				{
					outfile << "O";
				}
				if(j!=(XDiv-1))
					outfile << " ";
				circle(warped, Point(x, y), 3, Scalar(0, 0, 255), 3);
			}
			if(i!=YDiv)
				outfile << "\n";
		}
	}

	outfile.close();

	//If the amount of platforms is not correct
	if (foundBlack != amount)
		return -1;

	cv::imshow("warpedGray", warpedGray);
	cv::imshow("warped", warped);
	cv::waitKey();
	return 0;
}

extern "C" int __declspec(dllexport) __stdcall GetNewGrid(int platformAmount){
	Mat frame;

	capture >> frame;
	if(frame.empty())
		return -1;
	int result = findGrid(frame, 11, 7, platformAmount);
	return result;
}

extern "C" int __declspec(dllexport) __stdcall InitGridDetection()
{
	capture.open(0);

	if(!capture.isOpened())
		return -1;

	return 0;
}

extern "C" void __declspec(dllexport) __stdcall  CloseGridDetection()
{
	capture.release();
}