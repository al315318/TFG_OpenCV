#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"
#include <cmath>
#include <iostream>
#include <string>
#include <windows.h>

using namespace std;
using namespace cv;

//Saved rectangle reduction
const float REDUCFACTOR = 1.35f;

VideoCapture cap;

double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;

	return (dx1*dx2 + dy1*dy2) / sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255, 255, 255), CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);
}

int analysisPhoto(Mat frame, string path, int nRects, Size sizes[])
{
	cv::Scalar upperb = cv::Scalar(157, 157, 157);
	cv::Scalar lowerb = cv::Scalar(0,0,0);

	int b;

	cv::Mat src = frame;
	if (src.empty())
		return -1;

	//Convert to greyscale
	cv::Mat gray;
	cv::cvtColor(src, gray, CV_BGR2GRAY);

	// Detect image edges
	cv::Mat bw;
	cv::Canny(gray, bw, 0, 50, 5);

	// Look for contours
	std::vector<std::vector<cv::Point> > contours;
	cv::findContours(bw.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approx;
	cv::Mat dst = src.clone();

	vector<RotatedRect> minRect(nRects);
	int imageName = 0;

	for (size_t i = 0; i < contours.size(); i++)
	{
		// Reduce number of points using Douglas-Peucker algorithm
		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		// If it do not comply with these conditions it is an invalid object
		if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
			continue;

		// It's a triangle
		if (approx.size() == 3)
		{
			//setLabel(dst, "TRI", contours[i]);
		}
		else if (approx.size() >= 4 && approx.size() <= 6)
		{
			// Obtain the number of vertices
			int vtc = approx.size();

			// Get all the angles (cosines)
			std::vector<double> cos;
			for (int j = 2; j < vtc + 1; j++)
				cos.push_back(angle(approx[j%vtc], approx[j - 2], approx[j - 1]));

			//Sort the obtained angles
			std::sort(cos.begin(), cos.end());

			// Get the maximum and minimum angles
			double mincos = cos.front();
			double maxcos = cos.back();

			//Get the angles and determine the type of figure according
			//to angles obtained and the number of vertices
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3){
				setLabel(dst, "RECT", contours[i]);

				//Get the rotated rect
				minRect[imageName] = minAreaRect( Mat(contours[i]) );

				//Reduce size
				minRect[imageName].size = Size2f(minRect[imageName].size.width/REDUCFACTOR, minRect[imageName].size.height/REDUCFACTOR);
				imageName++;
			}
			else if (vtc == 5 && mincos >= -0.34 && maxcos <= -0.27)
				setLabel(dst, "PENTA", contours[i]);
			else if (vtc == 6 && mincos >= -0.55 && maxcos <= -0.45)
				setLabel(dst, "HEXA", contours[i]);
		}
		else
		{
			// Determine if it is a circle
			double area = cv::contourArea(contours[i]);
			cv::Rect r = cv::boundingRect(contours[i]);
			int radius = r.width / 2;

			if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 &&
				std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
				setLabel(dst, "CIR", contours[i]);
		}
	}

	//If all the desired elements have not been found, do nothing
	if (imageName!=nRects)
	{
		return -1;
	}

	//Save the new images
	for (int i = 0; i < imageName; i++)
	{
		// matrices we'll use
		Mat M, rotated, cropped, sized, mask, sizedGray, result;

		// get angle and size from the bounding box
		float angle = minRect[i].angle;
		Size rect_size = minRect[i].size;

		if (minRect[i].angle < -45.) {
			angle += 90.0;
			swap(rect_size.width, rect_size.height);
		}

		// get the rotation matrix
		M = getRotationMatrix2D(minRect[i].center, angle, 1.0);

		// perform the affine transformation
		warpAffine(src, rotated, M, src.size(), INTER_CUBIC);

		// crop the resulting image
		getRectSubPix(rotated, rect_size, minRect[i].center, cropped);

		//change size depending on body part
		Size size = sizes[i];

		cv::resize(cropped, sized, size);

		//Convert sized to grayscale
		cv::cvtColor(sized, sizedGray, CV_BGR2GRAY);

		if (i==0)
		{
			//sizedGray.convertTo(sizedGray, CV_8UC1);
			b = (int)sizedGray.at<uchar>(Point(5, 5));		//Aq�i se puede probar a 255-(int)...

			cv::imwrite("Assets\\Resources\\OpenCVSprites\\ImageB.png", sizedGray);
		}
		else
		{
			//Change alpha of white pixels to create mask
			if (b>=30)
			{
				lowerb = cv::Scalar(b-30, b-30, b-30);
			}
			else
			{
				lowerb = cv::Scalar(0, 0, 0);
			}

			if (b<=225)
			{
				upperb = cv::Scalar(b+30, b+30, b+30);	//Luz artificial: 157, 157, 157		Luz natural: 80, 80, 80			AUTO: b+100
			}
			else
			{
				lowerb = cv::Scalar(255, 255, 255);
			}

			//What is outside the two values will not be eliminated
			cv::inRange(sizedGray, lowerb, upperb, mask);

			cv::cvtColor(sized, result, CV_BGR2BGRA);

			for (int row = 0; row < result.rows; row++)
			{
				Vec4b* resultRow = result.ptr<Vec4b>(row);
				uchar* maskRow = mask.ptr<uchar>(row);

				for (int col = 0; col < result.cols; col++)
				{
					// Check if the pixel is inside the mask

					if (maskRow[col] == 255)
					{
						resultRow[col][3] = 0;
					}
				}
			}
			string fileName = path+std::to_string(i-1)+".png";
			//string fileName = "Assets\\Resources\\OpenCVSprites\\Image"+std::to_string(i-1)+std::to_string(path)+".png";

			cv::imwrite(fileName, result);
		}
	}

	// Draw rotated rects
	for( int i = 0; i< minRect.size(); i++ )
	{
		// rotated rectangle
		Point2f rect_points[4]; minRect[i].points( rect_points );
		for( int j = 0; j < 4; j++ )
			line( dst, rect_points[j], rect_points[(j+1)%4], Scalar(0, 255, 0), 1, 8 );
	}

	//cv::imshow("src", src);
	cv::imshow("dst", dst);
	cv::waitKey();
	return 0;
}

extern "C" int __declspec(dllexport) __stdcall GetNewCharacter(int character){
	Mat frame;
	int nRects = 7;
	string path = "Assets\\Resources\\OpenCVSprites\\Characters\\Image"+std::to_string(character);
	Size size [7] = {Size(10, 10), Size(56, 94), Size(56, 94), Size(97, 139),Size(64, 120), Size(64, 120), Size(102, 78)};

	cap >> frame;
	if(frame.empty())
		return -1;
	int result = analysisPhoto(frame, path, nRects, size);
	return result;
}

extern "C" int __declspec(dllexport) __stdcall GetNewScenery(int scenery){
	Mat frame;
	int nRects = 2;
	string path = "Assets\\Resources\\OpenCVSprites\\Scenery\\Image"+std::to_string(scenery);
	Size size [2] = {Size(10, 10), Size(1300, 1100)};

	cap >> frame;
	if(frame.empty())
		return -1;
	int result = analysisPhoto(frame, path, nRects, size);
	return result;
}

extern "C" void __declspec(dllexport) __stdcall  CloseElementDetection()
{
	cap.release();
}

extern "C" int __declspec(dllexport) __stdcall InitElementDetection()
{
	cap.open(0);

	if(!cap.isOpened())
		return -1;

	return 0;
}